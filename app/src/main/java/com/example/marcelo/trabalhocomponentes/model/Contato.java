package com.example.marcelo.trabalhocomponentes.model;

/**
 * Created by marcelo on 07/10/16.
 */

public class Contato {

    private static int contador = 0;
    private int id;
    private String nome;
    private boolean favorito;
    private int sexo;
    private int categoria;

    public boolean getFavorito() {
        return favorito;
    }

    public void setFavorito(boolean favorito) {
        this.favorito = favorito;
    }

    public Contato(String nome, String telefone, boolean favorito, int sexo, int categoria) {

        this.id = Contato.contador++;
        this.nome = nome;
        this.telefone = telefone;
        this.favorito = favorito;
        this.sexo = sexo;
        this.categoria = categoria;

    }

    private String telefone;

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }


    public int getContactId(){ return this.id; }

}
