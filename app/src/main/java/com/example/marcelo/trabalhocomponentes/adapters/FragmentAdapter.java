package com.example.marcelo.trabalhocomponentes.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.example.marcelo.trabalhocomponentes.fragments.ContatosFragment;
import com.example.marcelo.trabalhocomponentes.fragments.ExtrasFragment;

/**
 * Created by marcelo on 07/10/16.
 */

public class FragmentAdapter extends FragmentStatePagerAdapter {

    private String[] titulosTabs;

    public FragmentAdapter(FragmentManager fm, String[] titulosTabs) {
        super(fm);
        this.titulosTabs = titulosTabs;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0:
                return new ContatosFragment();
            case 1:
                return new ExtrasFragment();
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return titulosTabs.length;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return this.titulosTabs[position];
    }
}
