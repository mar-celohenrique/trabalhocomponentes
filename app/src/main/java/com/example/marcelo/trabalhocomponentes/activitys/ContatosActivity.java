package com.example.marcelo.trabalhocomponentes.activitys;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.example.marcelo.trabalhocomponentes.R;
import com.example.marcelo.trabalhocomponentes.controller.AdicionarContato;
import com.example.marcelo.trabalhocomponentes.database.BancoDeDadosContato;
import com.example.marcelo.trabalhocomponentes.keys.Codigos;
import com.example.marcelo.trabalhocomponentes.keys.Keys;
import com.example.marcelo.trabalhocomponentes.model.Contato;

import java.util.ArrayList;

public class ContatosActivity extends AppCompatActivity implements View.OnLongClickListener{


    private ListView listView;
    BancoDeDadosContato bancoDeDadosContato;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contatos);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        bancoDeDadosContato.getInstance();
        listView = (ListView) findViewById(R.id.lista_contatos);
        listView.setOnLongClickListener(this);

    }

    @Override
    protected void onStart() {

        super.onStart();

        ArrayList<Contato> list = BancoDeDadosContato.getContactList();
        refreshContactList( list );

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_contatos, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.adicionar_contato) {
            Intent intent = new Intent( this, AdicionarContato.class );
            startActivityForResult( intent, Codigos.REQUEST_ADD );
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        //Se a tela chamada foi a de adicionar
        if( requestCode == Codigos.REQUEST_ADD ){

            //Se o resultado da tela de adicionar foi ok
            if( resultCode == Codigos.RESPONSE_ADD_OK ){

                String nome = data.getExtras().getString( Keys.RESPONSE_SAVE_NOME );

                Context context = getApplicationContext();
                int duration = Toast.LENGTH_LONG;

                Toast toast = Toast.makeText(context, "Contato adicionado", duration);
                toast.show();


            } else if( resultCode == Codigos.RESPONSE_ADD_CANCEL ){
                Context context = getApplicationContext();
                int duration = Toast.LENGTH_LONG;
                Toast toast = Toast.makeText(context, "Operação cancelada", duration);
                toast.show();
            }
            //Implementar a volta da tela de edição
        } else if( requestCode == Codigos.REQUEST_EDIT ){


        }
    }

    private void refreshContactList( ArrayList<Contato> contactList ){

        final ArrayList<String> list = new ArrayList<String>();
        for ( Contato c: contactList ) {
            list.add( c.getNome() );
        }
        final ArrayAdapter adapter = new ArrayAdapter(this,
                android.R.layout.simple_list_item_1, list);
        listView.setAdapter(adapter);
        listView.setOnLongClickListener(this);
        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {

            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view,
                                           int position, long id) {
                // TODO Auto-generated method stub

                BancoDeDadosContato.getContactList().remove(position);

                adapter.notifyDataSetChanged();

                ArrayList<Contato> list = BancoDeDadosContato.getContactList();
                refreshContactList( list );

                Toast.makeText(ContatosActivity.this, "Contato deletado", Toast.LENGTH_LONG).show();

                return true;
            }

        });
    }

    @Override
    public boolean onLongClick(View v) {
        Toast.makeText(getApplicationContext(), "Click", Toast.LENGTH_LONG);
        return true;
    }


}
