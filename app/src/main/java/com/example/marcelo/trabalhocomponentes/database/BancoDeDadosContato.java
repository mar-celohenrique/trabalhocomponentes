package com.example.marcelo.trabalhocomponentes.database;

import com.example.marcelo.trabalhocomponentes.model.Contato;

import java.util.ArrayList;

/**
 * Created by marcelo on 07/10/16.
 */

public class BancoDeDadosContato {

    private static BancoDeDadosContato instance = null;
    private static ArrayList<Contato> listaDeContatos;

    private BancoDeDadosContato(){
        super();
        listaDeContatos = new ArrayList<Contato>();
    }

    public static BancoDeDadosContato getInstance(){

        if( instance == null ) instance = new BancoDeDadosContato();

        return instance;
    }

    public static void addContact( Contato contato ) {
        listaDeContatos.add( contato );
    }

    public static ArrayList<Contato> getContactList(){
        return listaDeContatos;
    }

    public static Contato getContactByName( String name ){

        for( Contato c: listaDeContatos ){
            if( c.getNome().equals( name ) ) return c;
        }

        return null;
    }


}
