package com.example.marcelo.trabalhocomponentes.controller;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.ToggleButton;

import com.example.marcelo.trabalhocomponentes.R;
import com.example.marcelo.trabalhocomponentes.database.BancoDeDadosContato;
import com.example.marcelo.trabalhocomponentes.keys.Codigos;
import com.example.marcelo.trabalhocomponentes.keys.Keys;
import com.example.marcelo.trabalhocomponentes.model.Contato;

public class AdicionarContato extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    BancoDeDadosContato bancoDeDadosContato;
    private Contato contato;
    private int sexo = 0;
    private int categoria = 0;

    private static final String[] DDD = new String[] {
            "88", "21", "11", "81", "85"
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_adicionar_contato);
        bancoDeDadosContato.getInstance();


        Spinner spinner = (Spinner) findViewById(R.id.tipo_telefone_spinner);
        ArrayAdapter<CharSequence> adapterSpinner = ArrayAdapter.createFromResource(this,
                R.array.spinner, android.R.layout.simple_spinner_item);
        adapterSpinner.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapterSpinner);
        spinner.setOnItemSelectedListener(this);

    }

    public void onRadioButtonClicked(View view) {
        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();

        // Check which radio button was clicked
        switch(view.getId()) {
            case R.id.radio_masculino:
                if (checked)
                    sexo = 1;
                    break;
            case R.id.radio_feminino:
                if (checked)
                    sexo = 2;
                    break;
        }
    }

    public void salvarContato( View view ){

        EditText nomeEditText = ( EditText )findViewById( R.id.editText_nome_contato);
        String nome = nomeEditText.getText().toString();

        EditText telefoneEditText = ( EditText )findViewById( R.id.editText_telefone_contato );
        String telefone = telefoneEditText.getText().toString();


        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_dropdown_item_1line, DDD);
        AutoCompleteTextView ddd = (AutoCompleteTextView)
                findViewById(R.id.ddd);
        ddd.setAdapter(adapter);

        String codigoArea = ddd.toString();

        final boolean[] favorito = {false};

        final ToggleButton toggle = (ToggleButton) findViewById(R.id.favorito_contato);
        toggle.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    favorito[0] = true;
                } else {
                    favorito[0] = false;
                }
            }
        });


        contato = new Contato(nome, codigoArea+telefone, favorito[0], sexo, categoria);
        bancoDeDadosContato.addContact(contato);
        Intent intentSalvar = new Intent();
        intentSalvar.putExtra( Keys.RESPONSE_SAVE_NOME, nome );
        setResult( Codigos.RESPONSE_ADD_OK, intentSalvar );
        finish();

    }

    public void cancelar( View view ){
        Intent intentCancelar = new Intent();
        setResult( Codigos.RESPONSE_ADD_CANCEL, intentCancelar);
        finish();
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        switch (position){
            case 0:
                categoria = 0;
                break;
            case 1:
                categoria = 1;
                break;
            case 2:
                categoria = 2;
                break;
            default:
                categoria = 3;
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
