package com.example.marcelo.trabalhocomponentes.keys;

/**
 * Created by marcelo on 07/10/16.
 */

public interface Codigos {

    public static final int REQUEST_ADD = 1;
    public static final int REQUEST_EDIT = 2;

    public static final int RESPONSE_ADD_OK = 3;
    public static final int RESPONSE_ADD_CANCEL = 4;

    public static final int RESPONSE_EDIT_OK = 5;
    public static final int RESPONSE_EDIT_CANCEL = 6;

}
