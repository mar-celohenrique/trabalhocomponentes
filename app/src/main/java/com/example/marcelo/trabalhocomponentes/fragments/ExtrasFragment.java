package com.example.marcelo.trabalhocomponentes.fragments;


import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.Toast;

import com.example.marcelo.trabalhocomponentes.R;
import com.example.marcelo.trabalhocomponentes.adapters.ImageAdapter;

/**
 * A simple {@link Fragment} subclass.
 */
public class ExtrasFragment extends Fragment {

    private MediaPlayer som;


    public ExtrasFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_extras, container, false);

        GridView gridview = (GridView) view.findViewById(R.id.gridview);
        gridview.setAdapter(new ImageAdapter(getActivity()));

        gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v,
                                    int position, long id) {
                if(position %2 ==0) {
                    som = MediaPlayer.create(getContext(), R.raw.latido1);
                    som.start();
                }else if(position %2 > 1){
                    som = MediaPlayer.create(getContext(), R.raw.latido2);
                    som.start();
                }else{
                    som = MediaPlayer.create(getContext(), R.raw.latido3);
                    som.start();
                }
            }
        });

        return view;
    }

}
